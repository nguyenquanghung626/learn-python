from rest_framework import serializers
from .models import Post, Person, Childen


class GetAllPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'
        # fields=('id','title', 'body','date')


class GetAllPersonSerializers(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = '__all__'


class GetAllChildenSerializers(serializers.ModelSerializer):
    class Meta:
        model = Childen
        fields = '__all__'

