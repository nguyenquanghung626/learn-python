from django.db import models


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Person(models.Model):
    firstName = models.CharField(max_length=10)
    midName = models.CharField(max_length=10)
    lastName = models.CharField(max_length=10)
    birthDay = models.DateTimeField(auto_now_add=True)
    age = models.IntegerField()

    def __str__(self):
        return self.firstName


class Childen(models.Model):
    firstName = models.CharField(max_length=10)
    midName = models.CharField(max_length=10)
    lastName = models.CharField(max_length=10)
    birthDay = models.DateTimeField(auto_now_add=True)
    age = models.IntegerField()

    def __str__(self):
        return self.firstName
