from django.contrib import admin
from .models import Post, Person, Childen


# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'date']
    list_filter = ['date']
    search_fields = ['id']


admin.site.register(Post)
admin.site.register(Person)
admin.site.register(Childen)
