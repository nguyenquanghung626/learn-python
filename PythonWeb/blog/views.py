from django.shortcuts import render
# fron django.http import
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from .models import Post, Person, Childen
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from blog.serializers import GetAllPostSerializer, GetAllPersonSerializers, GetAllChildenSerializers


# Create your views here.


# @csrf_exempt
def post_list(request):
    mydata = GetAllPostSerializer(data=request.data)
    if not mydata.is_valid():
        return Response("wrong", status=status.HTTP_400_BAD_REQUEST)
    title = mydata.data["title"]
    body = mydata.data["body"]
    ps = Post.objects.create(title=title, body=body)
    # return Response(data=ps.id, status=status.HTTP_200_OK)
    if request.method == 'GET':
        snippets = Post.objects.all()
        serializer = GetAllPostSerializer(snippets, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = GetAllPostSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

    ##################


class GetAllPostAPIView(APIView):

    def get(self, request):
        list_post = Post.objects.all()
        mydata = GetAllPostSerializer(list_post, many=True)
        return Response(data=mydata.data, status=status.HTTP_200_OK)

    def post(self, request):
        mydata = GetAllPostSerializer(data=request.data)
        if not mydata.is_valid():
            return Response("wrong", status=status.HTTP_400_BAD_REQUEST)
        title = mydata.data["title"]
        body = mydata.data["body"]
        ps = Post.objects.create(title=title, body=body)
        return Response(data=ps.id, status=status.HTTP_200_OK)


class GetAllPersonAPIView(APIView):

    def get(self, request):
        list_Name = Person.objects.all()
        mydata = GetAllPersonSerializers(list_Name, many=True)
        return Response(data=mydata.data, status=status.HTTP_200_OK)

    def post(self, request):
        mydata = GetAllPersonSerializers(data=request.data)
        if not mydata.is_valid():
            return Response("Wrong", status=status.HTTP_400_BAD_REQUEST)
        firstName = mydata.data["firstName"]
        midName = mydata.data["midName"]
        lastName = mydata.data["lastName"]
        age = mydata.data["age"]
        ps = Person.objects.create(firstName=firstName, midName=midName, lastName=lastName, age=age)
        return Response(data={ps.id, ps.firstName}, status=status.HTTP_200_OK)


class GetAllChildenAPIView(APIView):
    def get(self, request):
        list_Child = Childen.objects.all()
        mydata = GetAllChildenSerializers(list_Child, many=True)
        return Response(data=mydata.data, status=status.HTTP_200_OK)

    def post(self, request):
        mydata = GetAllChildenSerializers(data=request.data)
        if not mydata.is_valid():
            return Response("wrong", status=status.HTTP_400_BAD_REQUEST)
        firstName = mydata.data["firstName"]
        midName = mydata.data["midName"]
        lastName = mydata.data["lastName"]
        age = mydata.data["age"]
        ps = Childen.objects.create(firstName=firstName, midName=midName, lastName=lastName, age=age)
        return Response(data={ps.id, ps.age, ps.lastName}, status=status.HTTP_200_OK)

    def update(self, request):
        children = Childen.objects.get(id=1)
        mydata = GetAllChildenSerializers(children)
        if not mydata.is_valid():
            return Response("wrong", status=status.HTTP_400_BAD_REQUEST)

def home(request):
    return render(request,'pages/base1.html')