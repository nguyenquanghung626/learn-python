from django.urls import path
from blog import views
# from blog.views import GetAllPostAPIView, GetAllPersonAPIView, GetAllChildenAPIView

urlpatterns = [
    path('Post/', views.GetAllPostAPIView.as_view()),
    path('Person/', views.GetAllPersonAPIView.as_view()),
    path('child/', views.GetAllChildenAPIView.as_view()),
    path('homes/', views.home)
]
