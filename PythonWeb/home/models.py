from django.db import models

# Create your models here.
class Name(models.Model):
    firstName = models.CharField(max_length=200)
    midName = models.CharField(max_length=200)
    lastName = models.CharField(max_length=200)

    def __str__(self):
        return self.firstName
class Person(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField()

    def __str__(self):
        return self.name

