from django.shortcuts import render
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, RequestContext
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework import status
from .models import Name, Person
from .serializers import GetAllNameSerializers, GetAllPersonSerializers
from .forms import RegistrationForm
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import authenticate


# Create your views here.
def GetPerson(request):
    person_list = Person.objects.all()
    return render(request, 'pages/persondatabase.html', {'content': person_list})


def PostPerson(request):
    if request.method == 'POST':
        print(request)
        person_data = JSONParser().parse(request)
        person_serializer = GetAllPersonSerializers(data=person_data)
        if person_serializer.is_valid():
            person_serializer.save()
            return render(request, 'pages/base1.html')
            # return render(request, 'pages/persondatabase.html', Person.objects.all())
        return render(request, 'blog/base.html')


def index1(request):
    return render(request, 'pages/base1.html')


def baseBlog(request):
    return render(request, 'blog/base.html')


def viewsperson(request):
    return render(request, 'pages/persondatabase.html')


def addperson(request):
    return render(request, 'pages/insert.html')


@api_view(['GET', 'POST', 'DELETE'])
def name_list(request):
    if request.method == 'GET':
        names = Name.objects.all()
        title = request.query_params.get('title', None)
        if title is not None:
            names = names.filter(title__icontains=title)

        names_serializer = GetAllNameSerializers(names, many=True)
        return JsonResponse(names_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        name_data = JSONParser().parse(request)
        name_serializer = GetAllNameSerializers(data=name_data)
        if name_serializer.is_valid():
            name_serializer.save()
            return JsonResponse(name_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(name_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Name.objects.all().delete()
        return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def name_detail(request, pk):
    try:
        name = Name.objects.get(pk=pk)
    except Name.DoesNotExist:
        return JsonResponse({'message': 'The name does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        tutorial_serializer = GetAllNameSerializers(name)
        render(request, 'pages/persondetail.html', {tutorial_serializer.data})
        # return JsonResponse(tutorial_serializer.data)

    elif request.method == 'PUT':
        name_data = JSONParser().parse(request)
        name_serializer = GetAllNameSerializers(name, data=name_data)
        if name_serializer.is_valid():
            name_serializer.save()
            return JsonResponse(name_serializer.data)
        return JsonResponse(name_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        name.delete()
        return JsonResponse({'message': 'Name was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def person_list(request):
    content = {
        'user': str(request.user),  # `django.contrib.auth.User` instance.
        'auth': str(request.auth),  # None
    }
    if request.method == 'GET':
        persons = Person.objects.all()
        title = request.query_params.get('title', None)
        if title is not None:
            persons = persons.filter(title__icontains=title)

        persons_serializer = GetAllPersonSerializers(persons, many=True)
        return Response(content, persons_serializer.data)
        # return render(request, 'pages/persondatabase.html', {'content': persons})

    # elif request.method == 'POST':
    #     person = Person.objects.all()
    #     serializer = GetAllPersonSerializers(instance=person, data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return render(request, 'pages/persondatabase.html', {'content': Person.objects.all()})

    # elif request.method == 'DELETE':
    #     count = Person.objects.all().delete()
    #     return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])},
    #                         status=status.HTTP_204_NO_CONTENT)


@api_view(['PUT', 'DELETE'])
def person_detail(request, pk):
    try:
        person = Person.objects.get(id=pk)
    except ObjectDoesNotExist:
        return JsonResponse({'message': 'The person does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        person = Person.objects.get(id=pk)
        person_serializer = GetAllPersonSerializers(person, many=False)
        return Response(person_serializer.data)

    elif request.method == 'PUT':
        person_data = JSONParser().parse(request)
        person_serializer = GetAllPersonSerializers(person, data=person_data)
        if person_serializer.is_valid():
            person_serializer.save()
            return JsonResponse(person_serializer.data)
        return JsonResponse(person_serializer.errors, "status=status.HTTP_400_BAD_REQUEST")
    elif request.method == 'DELETE':
        person.delete()
        return Response('Delete complete')


def register(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/admin/auth/user/')
    return render(request, 'pages/register.html', {'form': form})
