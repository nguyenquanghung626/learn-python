from rest_framework import serializers
from .models import Name, Person


class GetAllNameSerializers(serializers.ModelSerializer):
    class Meta:
        model = Name
        fields = '__all__'

class GetAllPersonSerializers(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = '__all__'

