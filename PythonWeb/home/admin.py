from django.contrib import admin
from .models import Name, Person


# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display = ['id']


admin.site.register(Name)
admin.site.register(Person)
