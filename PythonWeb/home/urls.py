from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    # view normal
    path('normal-get/',views.GetPerson, name='normal-get-list-person'),
    path('normal-post/',views.PostPerson, name='normal-posts-list-person'),
    path('add-person/', views.addperson, name='normal-add-person'),

    # view API
    # path('', views.person_list, name='person-view'),
    path('create/', views.person_detail, name='person-create'),
    # path('', views.index),
    path('g/', views.index1),
    path('', views.addperson, name='add'),
    path('person/list/', views.person_list),
    path('pdetail/<str:pk>/', views.person_detail, name='person-detail'),
    path('register/', views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='pages/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
]
